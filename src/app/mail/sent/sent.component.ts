import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MailService } from '../../services/mail.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-sent',
  templateUrl: './sent.component.html',
  styleUrls: ['./sent.component.scss'],
})
export class SentComponent implements OnInit {
  inbox = [];

  constructor(
    private userService: UserService,
    private mailService: MailService,
    private activateRoute: ActivatedRoute,
    private snackBar: MatSnackBar,
  ) {}

  ngOnInit(): void {
    /* this.activateRoute.params.subscribe(params => {
      this.mailService.getInbox(params['userId']).subscribe(d => {
        this.inbox.push(d);
      }, err => this.snackBar.openFromComponent(AlertMessageComponent, {
          data: err,
          duration: 5000,
          verticalPosition: 'top',
        }));
    }); */
  }
}
