import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line new-cap
@Component({
  selector: 'app-menu-mailbox',
  templateUrl: './menu-mailbox.component.html',
  styleUrls: ['./menu-mailbox.component.scss'],
})
export class MenuMailboxComponent implements OnInit {
  menuIndex = 1;

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor() {}

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  ngOnInit(): void {}

  onMenuClick(menuIndex): void {
    this.menuIndex = menuIndex;
  }
}
