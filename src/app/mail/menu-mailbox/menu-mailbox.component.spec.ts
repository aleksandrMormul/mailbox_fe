/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MenuMailboxComponent } from './menu-mailbox.component';

describe('MenuMailboxComponent', () => {
  let component: MenuMailboxComponent;
  let fixture: ComponentFixture<MenuMailboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MenuMailboxComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuMailboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
