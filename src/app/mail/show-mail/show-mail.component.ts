import { ActivatedRoute } from '@angular/router';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { SharedService } from 'src/app/services/shared.service';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { MailService } from '../../services/mail.service';
import { ReceiverService } from '../../services/receiver.service';
import { Mail } from '../../interfaces/mail';
import { SharedUserService } from '../../services/sharedUser.service';

// eslint-disable-next-line new-cap
@Component({
  selector: 'app-show-mail',
  templateUrl: './show-mail.component.html',
  styleUrls: ['./show-mail.component.scss'],
})
export class ShowMailComponent implements OnInit, OnDestroy {
  mail = null;

  userEmail: string;

  emailSubject: string;

  panelOpenState = false;

  sharedSub: Subscription;

  userSharedSub: Subscription;

  constructor(
    private mailService: MailService,
    private sharedService: SharedService,
    private userSharedService: SharedUserService,
    private receiverService: ReceiverService,
    private activateRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.activateRoute.params.subscribe((params) => {
      this.sharedSub = this.sharedService.currentReceivers
        .pipe(filter((v) => v !== null))
        .subscribe((sh) => {
          this.userSharedSub = this.userSharedService.currentUsers
            .pipe(filter((v) => v !== null))
            .subscribe((users) => {
              const findReceiver = sh.filter((s) => s.mail_id === +params.mailId);
              const ownerId = findReceiver[0].owner;
              const findUser = users.filter((user) => user.id === ownerId);
              this.userEmail = findUser[0].email;
              this.mailService.getMail(params.mailId).subscribe((mail: Mail) => {
                this.receiverService.read(+params.mailId).subscribe();
                this.emailSubject = mail.subject;
                this.mail = mail;
              });
            });
        });
    });
  }

  ngOnDestroy(): void {
    if (this.sharedSub) {
      this.sharedSub.unsubscribe();
    }
    if (this.userSharedSub) {
      this.userSharedSub.unsubscribe();
    }
  }
}
