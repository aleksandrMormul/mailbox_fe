import { MatButtonModule } from '@angular/material/button';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MatMenuModule } from '@angular/material/menu';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { NewMailComponent } from './new-mail/new-mail.component';
import { AuthGuard } from '../guards/auth.guard';
import { MailComponent } from './mail.component';
import { MenuMailboxComponent } from './menu-mailbox/menu-mailbox.component';
import { InboxComponent } from './inbox/inbox.component';
import { SentComponent } from './sent/sent.component';
import { SpamComponent } from './spam/spam.component';
import { DraftComponent } from './draft/draft.component';
import { TrashComponent } from './trash/trash.component';
import { ShowMailComponent } from './show-mail/show-mail.component';

const itemRoutes: Routes = [
  { path: 'send/:userId', component: SentComponent, canActivate: [AuthGuard] },
  { path: 'spam', component: SpamComponent, canActivate: [AuthGuard] },
  { path: 'drafts', component: DraftComponent, canActivate: [AuthGuard] },
  { path: 'trash', component: TrashComponent, canActivate: [AuthGuard] },
  { path: 'new/:userId', component: NewMailComponent, canActivate: [AuthGuard] },
  {
    path: 'inbox',
    children: [
      { path: ':userId', component: InboxComponent, canActivate: [AuthGuard] },
      { path: 'mail/:mailId', component: ShowMailComponent, canActivate: [AuthGuard] },
    ],
  },
];

const routes: Routes = [
  { path: '', component: MailComponent, canActivate: [AuthGuard], children: itemRoutes },
];

// eslint-disable-next-line new-cap
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatButtonModule,
    MatGridListModule,
    MatListModule,
    MatSidenavModule,
    MatIconModule,
    MatFormFieldModule,
    MatSelectModule,
    MatTableModule,
    MatCardModule,
    MatExpansionModule,
    MatInputModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  declarations: [
    MailComponent,
    MenuMailboxComponent,
    InboxComponent,
    SentComponent,
    SpamComponent,
    DraftComponent,
    TrashComponent,
    NewMailComponent,
    ShowMailComponent,
  ],
})
export class MailModule {}
