import { Subscription } from 'rxjs';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { MailService } from '../../services/mail.service';
import { User } from '../../interfaces/user';

// eslint-disable-next-line new-cap
@Component({
  selector: 'app-new-mail',
  templateUrl: './new-mail.component.html',
  styleUrls: ['./new-mail.component.scss'],
})
export class NewMailComponent implements OnInit {
  // eslint-disable-next-line new-cap
  @Input() toUser: string;

  // eslint-disable-next-line new-cap
  @Input() mailSubject: string;

  // eslint-disable-next-line new-cap
  @Input() isDisabled = false;

  data = [];

  form: FormGroup;

  public userId: number;

  userEmail: string;

  emailCreateSub: Subscription;

  constructor(
    private userService: UserService,
    private fb: FormBuilder,
    private mailService: MailService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.userService.getInfoUser().subscribe((user: User) => {
      this.userEmail = user.email;
    });
    this.form = this.fb.group({
      email: [this.toUser, Validators.required],
      msg: ['', Validators.required],
      subject: ['', Validators.required],
    });
  }

  onFormSubmit(): void {
    if (!this.form.controls.email.value) {
      this.form.patchValue({
        email: this.toUser,
      });
    }
    if (!this.form.controls.subject.value) {
      this.form.patchValue({
        subject: this.mailSubject,
      });
    }
    const data = this.form.value;
    this.emailCreateSub = this.mailService.createMail(data).subscribe(
      () => {
        this.router.navigate([`mailbox/inbox/${this.userId}`]);
      },
      (err) => {
        console.log(err);
      },
    );
  }
}
