import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { ReceiverService } from '../services/receiver.service';
import { UserService } from '../services/user.service';
import { CommonService } from '../services/common.service';
import { Receiver } from '../interfaces/receiver';
import { SharedService } from '../services/shared.service';
import { User } from '../interfaces/user';
import { MailService } from '../services/mail.service';

// eslint-disable-next-line new-cap
@Component({
  selector: 'app-mail',
  templateUrl: './mail.component.html',
  styleUrls: ['./mail.component.scss'],
})
export class MailComponent implements OnInit, OnDestroy {
  // eslint-disable-next-line new-cap
  @ViewChild('drawer', { static: false }) drawer;

  userEmail: string;

  userId: number;

  countMails = 0;

  inboxSub: Subscription;

  receiverSub: Subscription;

  constructor(
    private commonService: CommonService,
    private userService: UserService,
    private sharedService: SharedService,
    private receiverService: ReceiverService,
    private mailService: MailService,
  ) {}

  ngOnInit(): void {
    this.inboxSub = this.userService.getInfoUser().subscribe((user: User) => {
      this.userEmail = user.email;
      this.userId = user.id;
      this.receiverSub = this.receiverService
        .getInbox()
        .pipe(filter((v) => v !== null))
        .subscribe((receiver: Receiver[]) => {
          this.sharedService.changeReceivers(receiver);
          const count = receiver.filter((isRead) => isRead.is_read === null);
          this.countMails = count.length;
        });
    });
  }

  logout(): void {
    this.sharedService.changeReceivers([]);
    this.mailService.resetInbox();
    this.receiverService.resetReceivers();
    this.sharedService.resetReceivers();
    this.commonService.logout();
  }

  ngOnDestroy(): void {
    if (this.inboxSub) {
      this.inboxSub.unsubscribe();
    }
    if (this.receiverSub) {
      this.receiverSub.unsubscribe();
    }
  }
}
