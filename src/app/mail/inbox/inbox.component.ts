import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SharedService } from 'src/app/services/shared.service';
import { Receiver } from 'src/app/interfaces/receiver';
import { Mail } from 'src/app/interfaces/mail';
import { UserService } from '../../services/user.service';
import { MailService } from '../../services/mail.service';
import { AlertMessageComponent } from '../../alert-message/alert-message.component';
import { User } from '../../interfaces/user';
import { SharedUserService } from '../../services/sharedUser.service';

// eslint-disable-next-line new-cap
@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class InboxComponent implements OnInit, OnDestroy {
  public inbox = null;

  selectedItem = '';

  userInbox: Subscription;

  mailIds = [];

  userIds = [];

  usersCollect: Subscription;

  mail: Mail[];

  receiver: Receiver[];

  shareSub: Subscription;

  constructor(
    private userService: UserService,
    private mailService: MailService,
    private userSharedService: SharedUserService,
    private sharedService: SharedService,
    private snackBar: MatSnackBar,
  ) {}

  ngOnInit(): void {
    this.shareSub = this.sharedService.currentReceivers
      .pipe(filter((v) => v !== null))
      .subscribe((receivers: Receiver[]) => {
        this.receiver = receivers;

        // eslint-disable-next-line no-restricted-syntax
        for (const inbox of receivers) {
          this.mailIds.push(inbox.mail_id);
          this.userIds.push(inbox.owner);
        }
        this.userIds = this.userIds.filter((value, index, arr) => arr.indexOf(value) === index);
        this.userInbox = this.mailService
          .getInbox(this.mailIds)
          .pipe(filter((v) => v !== null))
          .subscribe(
            (inbox) => {
              this.mail = inbox;
              this.getCollectUsers();
            },
            (err) =>
              this.snackBar.openFromComponent(AlertMessageComponent, {
                data: err,
                duration: 5000,
                verticalPosition: 'top',
              }),
          );
      });
  }

  getCollectUsers(): void {
    this.usersCollect = this.userService
      .getCollectUsers(this.userIds)
      .subscribe((users: User[]) => {
        this.userSharedService.changeUsers(users);
        users.forEach((user) => {
          this.receiver.forEach((inbox: Receiver) => {
            if (inbox.owner === user.id) {
              // eslint-disable-next-line no-param-reassign
              inbox.email = user.email;
              // eslint-disable-next-line no-param-reassign
              inbox.name = user.name;
            }
          });
        });
        const map = new Map();
        this.mail.forEach((item) => map.set(item.id, item));
        this.receiver.forEach((item) =>
          map.set(item.mail_id, {
            ...map.get(item.mail_id),
            ...item,
          }),
        );
        this.inbox = Array.from(map.values());
      });
  }

  handleClick(selectedItem): void {
    this.selectedItem = selectedItem;
  }

  ngOnDestroy(): void {
    if (this.shareSub) {
      this.shareSub.unsubscribe();
    }
    if (this.userInbox) {
      this.userInbox.unsubscribe();
    }
  }
}
