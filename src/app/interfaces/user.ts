export interface User {
  id: number;
  name: string;
  // eslint-disable-next-line camelcase
  last_name: string;
  email: string;
  password: string;
}
