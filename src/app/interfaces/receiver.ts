export interface Receiver {
  id: number;
  // eslint-disable-next-line camelcase
  mail_id: number;
  owner: number;
  recipient: number;
  // eslint-disable-next-line camelcase
  is_read: Date;
  // eslint-disable-next-line camelcase
  mail_type: string;
  starred: string;
  email: string;
  name: string;
}
