export interface Mail {
  id: number;
  body: string;
  subject: string;
}
