import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { CommonService } from '../services/common.service';

// eslint-disable-next-line new-cap
@Component({
  selector: 'app-register-in',
  templateUrl: './register-in.component.html',
  styleUrls: ['./register-in.component.css'],
})
export class RegisterInComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private commonService: CommonService,
    private router: Router,
  ) {}

  initForm(): void {
    this.form = this.fb.group({
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      email: [
        null,
        [
          Validators.required,
          Validators.email,
          // eslint-disable-next-line no-useless-escape
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),
        ],
      ],
      password: [null, [Validators.required, Validators.minLength(6)]],
      confirmPassword: [null, [Validators.required, Validators.minLength(6)]],
    });
  }

  ngOnInit(): void {
    this.initForm();
  }

  onFormSubmit(): void {
    if (this.form.invalid) {
      return;
    }
    this.authService.signUp(this.form.value).subscribe(
      () => {
        alert('User Registered successfully!!');
        this.router.navigate(['/mailbox']);
      },
      (error) => {
        console.log(error);
        const e = JSON.parse(error.error);
        this.commonService.showErrorMessage(e);
      },
    );
  }
}
