import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterInComponent } from './register-in/register-in.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path: 'mailbox',
    loadChildren: () => import('./mail/mail.module').then((m) => m.MailModule),
  },
  { path: '', pathMatch: 'prefix', redirectTo: 'login' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterInComponent },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
