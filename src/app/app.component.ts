import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AlertMessageComponent } from './alert-message/alert-message.component';
import { CommonService } from './services/common.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'mail-frontend';

  constructor(private commonService: CommonService, private snackBar: MatSnackBar) {}

  ngOnInit() {
    this.commonService.message.subscribe((value) => {
      if (value != null) {
        this.snackBar.openFromComponent(AlertMessageComponent, {
          data: value,
          duration: 5000,
          verticalPosition: 'top',
        });
      }
    });
  }
}
