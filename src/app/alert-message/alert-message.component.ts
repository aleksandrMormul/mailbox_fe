import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material/snack-bar';

@Component({
  selector: 'app-alert-message',
  templateUrl: './alert-message.component.html',
  styleUrls: ['./alert-message.component.scss'],
  // tslint:disable-next-line:use-component-view-encapsulation
  encapsulation: ViewEncapsulation.None,
})
export class AlertMessageComponent implements OnInit {
  actionButtonText = 'OK';

  constructor(
    // tslint:disable-next-line:no-any
    @Inject(MAT_SNACK_BAR_DATA) public data: any,
    private snackBarRef: MatSnackBarRef<AlertMessageComponent>,
  ) {}

  // eslint-disable-next-line @angular-eslint/no-empty-lifecycle-method,class-methods-use-this
  ngOnInit(): void {}

  onActionButtonClicked() {
    this.snackBarRef.dismiss();
  }
}
