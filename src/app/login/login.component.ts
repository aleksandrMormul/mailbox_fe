import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
import { CommonService } from '../services/common.service';
import { AuthService } from '../services/auth.service';
import { Auth } from '../interfaces/auth';
import { User } from '../interfaces/user';

// eslint-disable-next-line new-cap
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  form: FormGroup;

  userId: number;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private userService: UserService,
    private commonService: CommonService,
  ) {}

  initForm(): void {
    this.form = this.fb.group({
      email: [
        null,
        [
          Validators.required,
          Validators.email,
          // eslint-disable-next-line no-useless-escape
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),
        ],
      ],
      password: [null, Validators.required],
    });
  }

  ngOnInit(): void {
    this.initForm();
  }

  onFormSubmit(): void {
    if (this.form.invalid) {
      return;
    }
    this.authService.login(this.form.value).subscribe(
      (auth: Auth) => {
        this.commonService.localStorage.setItem('authorization', auth.token);
        this.userService.getInfoUser().subscribe((user: User) => {
          this.userId = user.id;
          this.router.navigate([`mailbox/inbox/${this.userId}`]);
        });
      },
      () => {
        alert('ERROR!!');
      },
    );
  }
}
