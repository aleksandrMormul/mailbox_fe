import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private httpClient: HttpClient) {}

  public signUp(data) {
    const url = `${environment.apiUrl}/api/auth/register`;
    const reqBody = {
      name: data.firstName,
      email: data.email,
      last_name: data.lastName,
      password: data.password,
      password_confirmation: data.confirmPassword,
    };
    return this.httpClient.post(url, reqBody).pipe();
  }

  public login(data) {
    const url = `${environment.apiUrl}/api/auth/login`;
    const reqBody = {
      email: data.email,
      password: data.password,
    };
    return this.httpClient.post(url, reqBody).pipe();
  }

  public refreshToken(token) {
    const headers = { Authorization: `Bearer ${token}` };
    const url = `${environment.apiUrl}/api/auth/refresh`;
    return this.httpClient.post(url, '', { headers }).pipe();
  }
}
