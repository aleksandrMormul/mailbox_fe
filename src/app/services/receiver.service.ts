import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { environment } from '../../../environment';
import { Receiver } from '../interfaces/receiver';

@Injectable({
  providedIn: 'root',
})
export class ReceiverService {
  private receiver$: BehaviorSubject<Receiver[]> = new BehaviorSubject(null);

  constructor(private httpClient: HttpClient) {}

  public getInbox(type?) {
    let url = '';
    if (type) {
      url = `${environment.apiUrl}/api/receivers/?type=${type}`;
    } else {
      url = `${environment.apiUrl}/api/receivers/?type=`;
    }
    if (this.receiver$.getValue() === null) {
      this.httpClient.get<Receiver[]>(url).subscribe((inbox) => {
        this.receiver$.next(inbox);
      });
    }
    return this.receiver$.asObservable();
  }

  resetReceivers(): void {
    this.receiver$.next(null);
  }

  public read(mailId: number) {
    const url = `${environment.apiUrl}/api/reads`;
    return this.httpClient.put(url, { mailId: +mailId }).pipe(
      tap(() => {
        const newReceiver = [...this.receiver$.getValue()];
        const rec = newReceiver.find((nr) => nr.mail_id === +mailId);
        if (rec.is_read === null) {
          rec.is_read = new Date();
        }
        this.receiver$.next(newReceiver);
      }),
    );
  }
}
