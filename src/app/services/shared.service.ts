import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Receiver } from '../interfaces/receiver';

@Injectable({
  providedIn: 'root',
})
export class SharedService {
  private receivers$ = new BehaviorSubject<Receiver[]>(null);

  currentReceivers = this.receivers$.asObservable();

  changeReceivers(receivers: Receiver[]) {
    if (this.receivers$.getValue() === null) {
      this.receivers$.next(receivers);
    } else {
      this.receivers$.asObservable();
    }
  }

  resetReceivers(): void {
    this.receivers$.next(null);
  }
}
