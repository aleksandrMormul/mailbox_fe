import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environment';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  headers: HttpHeaders;

  constructor(private httpClient: HttpClient) {}

  public getInfoUser(): Observable<User> {
    const url = `${environment.apiUrl}/api/auth/user-profile`;
    return this.httpClient.get<User>(url).pipe();
  }

  public getUsers(): Observable<User[]> {
    const url = `${environment.apiUrl}/api/users`;
    return this.httpClient.get<User[]>(url).pipe();
  }

  public getCollectUsers(data): Observable<User[]> {
    const url = `${environment.apiUrl}/api/collectusers`;
    return this.httpClient.get<User[]>(url, {
      params: { ids: JSON.stringify(data) },
    });
  }

  public getUser(id): Observable<User> {
    const url = `${environment.apiUrl}/api/users/${id}`;
    return this.httpClient.get<User>(url).pipe();
  }
}
