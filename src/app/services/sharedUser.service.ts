import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../interfaces/user';

// eslint-disable-next-line new-cap
@Injectable({
  providedIn: 'root',
})
export class SharedUserService {
  private users$ = new BehaviorSubject<User[]>(null);

  currentUsers = this.users$.asObservable();

  changeUsers(receivers: User[]) {
    if (this.users$.getValue() === null) {
      this.users$.next(receivers);
    } else {
      this.users$.asObservable();
    }
  }
}
