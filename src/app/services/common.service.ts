import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  localStorage: Storage;

  message = new BehaviorSubject(null);

  constructor(private router: Router) {
    this.localStorage = window.localStorage;
  }

  getLocalStorageItem(key: string) {
    return this.localStorage.getItem(key);
  }

  getAuthToken() {
    return this.getLocalStorageItem('authorization') || null;
  }

  removeSessionStorageItem(key: string): void {
    this.localStorage.removeItem(key);
  }

  clearSessionStorage(): void {
    this.localStorage.clear();
  }

  logout() {
    this.clearSessionStorage();
    this.router.navigate(['/login']);
  }

  public showErrorMessage(messageContent, errorCode?) {
    const msg = messageContent;

    // eslint-disable-next-line no-restricted-syntax
    for (const m in msg) {
      if (Object.prototype.hasOwnProperty.call(msg, m)) {
        this.message.next({
          type: 'error',
          message: `${msg[m]}`,
          code: errorCode || null,
        });
      }
    }
  }
}
