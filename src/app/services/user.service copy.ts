import { filter } from 'rxjs/operators';
import { CommonService } from './common.service';
import { environment } from './../../../environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  headers: HttpHeaders;
  private users$: BehaviorSubject<any> = new BehaviorSubject(null);
  private user$: BehaviorSubject<any> = new BehaviorSubject(null);

  constructor(private httpClient: HttpClient, private commonService: CommonService) {}

  public getInfoUser(): Observable<any> {
    //this.headers = new HttpHeaders({ 'Authorization': 'Bearer' + " " + this.commonService.getAuthToken() });
    const url = environment.apiUrl + '/api/auth/user-profile';
    return this.httpClient.get(url).pipe();
  }

  public getUsers(): Observable<any> {
    const url = environment.apiUrl + '/api/users';
    this.httpClient.get(url).subscribe((u) => {
      this.users$.next(u);
    });
    return this.users$.asObservable();
  }

  public getUser(id): Observable<any> {
    console.log(' 2 value user = ', this.users$.getValue());
    if (this.users$.getValue() !== null) {
      const user = this.users$.getValue().filter((u) => u.id === id);
      this.user$.next(user);
      return this.user$.asObservable();
    }
    const url = environment.apiUrl + '/api/user/' + id;
    if (this.users$.getValue() == null || this.users$.getValue().getValue().id !== id) {
      console.log(' 1 value user = ', this.users$.getValue());
      this.httpClient.get(url).subscribe((u) => {
        this.user$.next(u);
      });
    }
    console.log(' 3 value user = ', this.users$.getValue());
    return this.user$.asObservable();
  }
}
