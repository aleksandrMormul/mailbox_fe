import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../../environment';
import { Mail } from '../interfaces/mail';

@Injectable({
  providedIn: 'root',
})
export class MailService {
  headers: HttpHeaders;

  private mails$: BehaviorSubject<Mail[]> = new BehaviorSubject(null);

  constructor(private httpClient: HttpClient) {}

  public createMail(data) {
    const url = `${environment.apiUrl}/api/creates`;
    const reqBody = {
      to: data.email,
      subject: data.subject,
      msg: data.msg,
      user: data.user,
      user_id: data.userId,
      is_read: false,
      status: 'send',
    };
    return this.httpClient.post(url, reqBody).pipe();
  }

  public getInbox(ids) {
    const url = `${environment.apiUrl}/api/inboxs/?type=`;
    if (this.mails$.getValue() === null) {
      this.httpClient
        .get(url, {
          params: { ids: JSON.stringify(ids) },
        })
        .subscribe((inbox: Mail[]) => {
          this.mails$.next(inbox);
        });
    }
    return this.mails$.asObservable();
  }

  resetInbox(): void {
    this.mails$.next(null);
  }

  public getMail(id): Observable<Mail> {
    const url = `${environment.apiUrl}/api/mails/${id}`;
    return this.httpClient.get<Mail>(url);
  }
}
