/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ReceiverService } from './receiver.service';

describe('Service: Receiver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReceiverService],
    });
  });

  it('should ...', inject([ReceiverService], (service: ReceiverService) => {
    expect(service).toBeTruthy();
  }));
});
